<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<html>
<head>
<title>Order history</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage users</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/w3.css"></link>

<style>
td div.manage-link {
	display: none;
}

tbody tr:hover td div.manage-link {
	display: block;
	position: absolute;
	background-color: #f9f9f9;
	height: 50%;
	width: 56%;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	padding: 12px 16px;
	z-index: 1;
}
</style>
</head>

<body>

	<div class="header">

		<div class="container">

			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					</form>	
				</ul>
			</div>
		</div>
		<div class="check-out">
			<div class="container">
				<div class="spec ">
					<h3>Order history</h3>
					<div class="ser-t">
						<b></b> <span><i></i></span> <b class="line"></b>
					</div>
				</div>
				<table class="table">
					<tbody>
						<tr>
							<th class="t-head">Order number</th>
							<th class="t-head">Date</th>
							<th class="t-head">Status</th>
							<th class="t-head">Price</th>
						</tr>
						<c:forEach var="order" items="${orders}">
							<tr>
								<td class="t-data">${order.id}
									<div class="manage-link">
										<table class="table">
											<tr>
												<th class="t-head head-it ">Products</th>
												<th class="t-head">Price</th>
												<th class="t-head">Quantity</th>
												<th class="t-head">Total price</th>
											</tr>
											<c:forEach var="product" items="${order.products}">
												<tr>
													<td class="ring-in t-data"><a href="#" class="at-in">
															<img src="images/wi.png" class="img-responsive" alt="">
													</a>
														<div class="sed">
															<h5>${product.name}</h5>
														</div>
														<div class="clearfix"></div></td>
													<td class="t-data">${product.price}</td>
													<td class="t-data">${product.quantity}</td>
													<td class="t-data">${product.totalPrice}</td>
												</tr>
											</c:forEach>
										</table>
									</div>
								</td>
								<td class="t-data">${order.date}</td>
								<td class="t-data">${order.status}</td>
								<td class="t-data">${order.price}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
</body>
</html>