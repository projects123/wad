<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>

</head>
<body>



  			 <sql:setDataSource var="myDS" driver="com.mysql.jdbc.Driver"
  		        url="jdbc:mysql://localhost/wad" user="root" password="" />
  		        
  			<sql:query var="listCart"   dataSource="${myDS}">
  		        SELECT * FROM cart WHERE email = ?
  		        <sql:param value = "${globalEmail}" />
  		    </sql:query>

<div class="header">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="profile" name="profile" class='btn btn-sm btn-primary-outline' id='profile'><span class="glyphicon glyphicon-log-in"></span> Profile </button></li>		
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					</form>					
				</ul>		
			</div>
		</div>
					<div class="check-out">	 
		<div class="container">	 
	 <div class="spec ">
				<h3>Wishlist</h3>
					<div class="ser-t">
						<b></b>
						<span><i></i></span>
						<b class="line"></b>
					</div>
			</div>
			
			
			
 <table class="table ">
		  <tr>
			<th class="t-head head-it ">Products</th>
			<th class="t-head">Price</th>
			<th class="t-head">Quantity</th>
			<th class="t-head">Total price</th>
		  </tr>
		  <tr>
		  
		  <c:forEach var="cart" items="${listCart.rows}">
		  <tr>
			<td class="ring-in t-data">
				<a href="#" class="at-in">
					<img src="${cart.imageURL}" class="img-responsive" alt="" width="75" height="75">
				</a>
			<div class="sed">
				<h5>${cart.productName}</h5>
			</div>
				<div class="clearfix"> </div>
				<div class="close1">
				<form action="CartController" method="post"> 
				<input type="hidden" name="email" value="${globalEmail}" />							
				<input type="hidden" name="imagine" value="${cart.imageURL}" />
				<input type="hidden" name="productName" value="${cart.productName}" />
				<input type="hidden" name="price" value="${cart.price}" />
				<input type="hidden" name="email" value="${globalEmail}" />		
				 <button type="submit" value="delete" name="delete" class='btn btn-sm btn-primary-outline' id='delete'><span class="glyphicon glyphicon-trash"></span></button> </div>
				 </form>
			 </td>
			<td class="t-data">${cart.price}</td>
			<td class="t-data"><div class="quantity"> 
								<div class="quantity-select">            
									<div class="entry value-minus">&nbsp;</div>
										<div class="entry value"><span class="span-1">1</span></div>									
									<div class="entry value-plus active">&nbsp;</div>
								</div>
							</div>
			
			</td>
			<td class="t-data">${cart.price}</td>
			</tr>
			</c:forEach>
	</table>
		 </div>
		 
		 <label>Total</label>
		 <button>Order</button>
		 </div>
			

     

</body>
</html>