<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>

</head>
<body>



  			 <sql:setDataSource var="myDS" driver="com.mysql.jdbc.Driver"
  		        url="jdbc:mysql://localhost/wad" user="root" password="" />
  		        
  			<sql:query var="listProduct"   dataSource="${myDS}">
  		        SELECT * FROM product WHERE category = ? and ? <= price and price <= ? or name  = ?
  		        <sql:param value = "${globalCategory}" />
  		        <sql:param value = "${globalPriceMin}" />
  		        <sql:param value = "${globalPriceMax}" />
  		        <sql:param value = "${globalSearch}" />
  		    </sql:query>
  		    
<div class="header">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="profile" name="profile" class='btn btn-sm btn-primary-outline' id='profile'><span class="glyphicon glyphicon-log-in"></span> Profile </button></li>		
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					</form>					
				</ul>		
			</div>
		</div>

	</div>

	      
	<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:10%">
<h3 class="w3-bar-item">Sort</h3>
  <div class="w3-bar-item">Category</div>
  <div class="w3-bar-item">
  <form action="HomeController" method="post">  
		<select name="category">
		  <option value="Category1">Category1</option>
		  <option value="Category2">Category2</option>
		  <option value="Category3">Category3</option>
		  <option value="Category4">Category4</option>
		</select>
	</div>
	
  <div class="w3-bar-item">Name</div>
  <div class="w3-bar-item"><input value="name" name="name" class="round-corner" type='text' placeholder="Name" style='width:100%'></div>
  <div class="w3-bar-item">Price</div>
  <div class="w3-bar-item"><input value="0" name="priceMin" class="round-corner" type='text' placeholder="Min" style='width:30%'>-<input value="9999" name="priceMax" class="round-corner" type='text'placeholder="Max" style='width:30%'></div>
  <div class="w3-bar-item"><button type="submit" value="sort" name="sort" class="btn btn-danger">Sort</button></div>
</form>
  </div>
  

  
   
  
<div style="margin-left:10%">
		<div class="product">
		<div class="container">
			<div class="spec ">
				<h3>Products</h3>
				<div class="ser-t">
					<b></b>
					<span><i></i></span>
					<b class="line"></b>
				</div>
			</div>
				<div class=" con-w3l agileinf">
				
					<c:forEach var="product" items="${listProduct.rows}">
							<div class="col-md-3 pro-1">
								<div class="col-m">

								<a href="#" class="offer-img">
										<img value="${product.imageURL}" name="imagine" src="${product.imageURL}" class="img-responsive" alt="">
									</a>
									<div class="mid-1">
										<div>
											<h6><a href="#">${product.name}</a></h6>							
										</div>
										<div class="mid-2">
											<p><em class="item_price">${product.price}</em></p>
											  
											<div class="clearfix"></div>
										</div>
							<form action="HomeController" method="post"> 
										<div class="view">
												<input type="hidden" name="email" value="${globalEmail}" />										
												<input type="hidden" name="imagine" value="${product.imageURL}" />
												<input type="hidden" name="productName" value="${product.name}" />
												<input type="hidden" name="price" value="${product.price}" />
												<input type="hidden" name="description" value="${product.description}" />
												<button stype="submit" value="produce" name="produce"  class="btn btn-danger">View</button>

												<input type="hidden" name="email" value="${globalEmail}" />										
												<input type="hidden" name="imagine" value="${product.imageURL}" />
												<input type="hidden" name="productName" value="${product.name}" />
												<input type="hidden" name="price" value="${product.price}" />											
												<button stype="submit" value="cart" name="cart" class="btn btn-danger">Add to Cart</button>
											</div>
								
									</div>
									</form>	
									
								</div>
							</div>							
							
						</c:forEach>
						<div class="clearfix"></div>
					</div>
		</div>
	</div>
</div>
</body>
</html>