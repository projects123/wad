<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Manage Orders</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/w3.css"></link>

<!--link href="css/bootstrap.css" rel='stylesheet' type='text/css' /-->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!--script src="js/jquery-1.11.1.min.js"></script-->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<style>
td div.manage-link {
	display: none;
}

tbody tr:hover td div.manage-link {
	display: block;
	position: absolute;
	background-color: #f9f9f9;
	height: 50%;
	width: 56%;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	padding: 12px 16px;
	z-index: 1;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	height: 600px;
	width: 400px;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	padding: 12px 16px;
	z-index: 1;
}

.dropdown:hover .dropdown-content {
	display: block;
}
</style>
</head>

<body>

	<div class="header">

				<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="ManageUserController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="profile" name="profile" class='btn btn-sm btn-primary-outline' id='profile'><span class="glyphicon glyphicon-log-in"></span> Profile </button></li>		
					<li><button type="submit" value="manageorders" name="manageorders" class='btn btn-sm btn-primary-outline' id='manageOrders'><span class="glyphicon glyphicon-log-in"></span> Manage Orders</button></li>
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					</form>					
				</ul>		
			</div>
		
		</div>
	</div>

	<div class="check-out">
		<div class="w3-sidebar w3-light-grey w3-bar-block"
			style="width: 10%; height: 35%;">
			<h3 class="w3-bar-item">Sort</h3>
			<div class="w3-bar-item">Price</div>
			<div class="w3-bar-item">
				<input class="round-corner" type='text' placeholder="Min"
					style='width: 30%'>-<input class="round-corner" type='text'
					id='searchText' placeholder="Max" style='width: 30%'>
			</div>
			<div class="w3-bar-item">Date</div>
			<div class="w3-bar-item">
				<input class="round-corner" type='text' placeholder="Start date"
					style='width: 100%'>
			</div>
			<div class="w3-bar-item">
				<input class="round-corner" type='text' placeholder="End date"
					style='width: 100%'>
			</div>

			<div class="w3-bar-item">
				<button class="btn btn-danger">Sort</button>
			</div>
		</div>
		<div class="container" style="margin-left: 10%">
			<div class="spec ">
				<h3>Unprocessed orders</h3>
				<div class="ser-t">
					<b></b> <span><i></i></span> <b class="line"></b>
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<th class="t-head">Order number</th>
						<th class="t-head">Date</th>
						<th class="t-head">Price</th>
						<th class="t-head"></th>
					</tr>
					<c:forEach var="unprocessedOrder" items="${unprocessedOrders}">
						<tr>
							<td class="t-data" name="orderId">${unprocessedOrder.id}
								<div class="manage-link" >
									<table class="table">
										<tr>
											<th class="t-head head-it ">Products</th>
											<th class="t-head">Price</th>
											<th class="t-head">Quantity</th>
											<th class="t-head">Total price</th>
										</tr>
										<c:forEach var="product" items="${unprocessedOrder.products}">
											<tr>
												<td class="ring-in t-data"><a href="#" class="at-in">
														<img src="images/wi.png" class="img-responsive" alt="">
												</a>
													<div class="sed">
														<h5>${product.name}</h5>
													</div>
													<div class="clearfix"></div></td>
												<td class="t-data">${product.price}</td>
												<td class="t-data">${product.quantity}</td>
												<td class="t-data">${product.totalPrice}</td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</td>
							<td class="t-data">${unprocessedOrder.date}</td>
							<td class="t-data">${unprocessedOrder.price}</td>
							<td class="t-data"><button
									type="submit" value="Accept" name="Accept"  id='Accept' class='btn btn-sm btn-primary-outline'>
									<span class="glyphicon glyphicon-ok"></span>
								</button>
								<button type="submit" value="Remove" name="Remove" id="Remove" class='btn btn-sm btn-primary-outline'>
									<span class="glyphicon glyphicon-remove"></span>
								</button></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
		<div class="container" style="margin-left: 10%">
			<div class="spec ">
				<h3>Processed orders</h3>
				<div class="ser-t">
					<b></b> <span><i></i></span> <b class="line"></b>
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<th class="t-head">Order number</th>
						<th class="t-head">Date</th>
						<th class="t-head">Price</th>
						<th class="t-head"></th>
					</tr>
					<c:forEach var="processedOrder" items="${processedOrders}">
						<form action="ManageOrdersController" method="post">
						<tr>
							<td class="t-data">${processedOrder.id}
								<div class="manage-link">
									<table class="table">
										<tr>
											<th class="t-head head-it ">Products</th>
											<th class="t-head">Price</th>
											<th class="t-head">Quantity</th>
											<th class="t-head">Total price</th>
										</tr>
										<c:forEach var="product" items="${processedOrder.products}">
											<tr>
												<td class="ring-in t-data"><a href="#" class="at-in">
														<img src="images/wi.png" class="img-responsive" alt="">
												</a>
													<div class="sed">
														<h5>${product.name}</h5>
													</div>
													<div class="clearfix"></div></td>
												<td class="t-data">${product.price}</td>
												<td class="t-data">${product.quantity}</td>
												<td class="t-data">${product.totalPrice}</td>
											</tr>
										</c:forEach>

									</table>
								</div>
							</td>
							<td class="t-data">${processedOrder.date}</td>
							<td class="t-data">${processedOrder.price}</td>

						</tr>
						</form>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>



</body>
</html>