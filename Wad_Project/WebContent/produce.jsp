<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>

</head>
<body>
  		    
<div class="header">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="profile" name="profile" class='btn btn-sm btn-primary-outline' id='profile'><span class="glyphicon glyphicon-log-in"></span> Profile </button></li>		
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					</form>				
				</ul>		
			</div>
		</div>

	</div>

			<div class="single">
			<div class="container">
						<div class="single-top-main">
	   		<div class="col-md-5 single-top">
	   		<div class="single-w3agile">
							
<div id="picture-frame">
			<img src="${globalImagine}" data-src="images/si-1.jpg" alt="" class="img-responsive"/>
		</div>		
			</div>
			</div>
			<div class="col-md-7 single-top-left ">
								<div class="single-right">
				<h3>${globalProductName}</h3>
				
					
				 <div class="pr-single">
				  <p >${globalPrice}</p>
				</div>
				<div class="block block-w3">
					<div class="starbox small ghosting"> </div>
				</div>
				<p class="in-pa"> ${globalDescription} </p>
			   	
				<form action="ProduceController" method="post"> 
					<div class="add add-3">	
									<input type="hidden" name="email" value="${globalEmail}" />							
									<input type="hidden" name="imagine" value="${globalImagine}" />
									<input type="hidden" name="productName" value="${globalProductName}" />
									<input type="hidden" name="price" value="${globalPrice}" />
									<button type="submit" value="cart" name="cart" class="btn my-cart-btn">Add to Cart</button>
					</div>
				</form>
				 
				<div>
					 <ol style="width:100%;height:100px;overflow: auto;">
						<c:forEach var="comment" items="${commetns}">
							<li>${comment.comment }</li>
						</c:forEach>
					
					</ol>
				</div>
			<div class="clearfix"> </div>
			</div>
		 

			</div>
		   <div class="clearfix"> </div>
	   </div>	
		<div class="forg" >
					<form action="ProduceController" method="post"> 
					<input value="" name="commentProduct" class="round-corner" type='text'  style='width:80%'>
					<input type="hidden" name="email" value="${globalEmail}" />										
					<input type="hidden" name="imagine" value="${globalImagine}" />
					<input type="hidden" name="productName" value="${product.name}" />
					<input type="hidden" name="price" value="${globalPrice}" />
					<input type="hidden" name="description" value="${globalDescription}" />
					<button type="submit" value="comment" name="comment" class='btn btn-sm btn-primary-outline forg-right'  style='vertical-align:middle; line-height:90px'> Add comment </button>
					</form>
				<div class="clearfix"></div>
				</div>
				
	</div>
</div>
     
</body>
</html>