<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<html>
<head>
<title>ManageProducts</title>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/css/w3.css"></link>
<style>
td div.manage-link {
	display: none;
}

tbody tr:hover td div.manage-link {
	display: block;
	position: absolute;
	background-color: #f9f9f9;
	height: 50%;
	width: 56%;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	padding: 12px 16px;
	z-index: 1;
}

.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	height: 600px;
	width: 400px;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	padding: 12px 16px;
	z-index: 1;
}

.dropdown:hover .dropdown-content {
	display: block;
}
</style>
</head>

<body>

	<div class="header">

		<div class="container">

			<div class="head-t">
					<form action="ManageProductsController" method="post">
				<ul class="card">
						<li><button class='btn btn-sm btn-primary-outline' id='home'>
								<span class="glyphicon glyphicon-home"></span> Home
							</button></li>
						<li><input class="round-corner" type='text' id='searchText'
							placeholder="Search" style="width: 70%"></li>
						<li><button class='btn btn-sm btn-primary-outline'
								id='search'>
								<span class="glyphicon glyphicon-search"></span> Search
							</button></li>
						<li><button class='btn btn-sm btn-primary-outline' id='login'>
								<span class="glyphicon glyphicon-log-in"></span> Login
							</button></li>
				</ul>
				</form>
			</div>
		</div>
	</div>

	<button class="btn btn-danger dropdown" style="margin-left: 10%">
		Add new product
		<div class="dropdown-content">
<form action="ManageProductsController" method="post">
						
			<div class="form-w3agile form1">
				<h3>New Product</h3>
				<div class="key">
					<input type="text" value="Name" name="NewName">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<input type="text" value="Description" name="NewDescription">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<input type="text" value="Price" name="NewPrice">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<input type="text" value="Stock" name="NewStock">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<input type="text" value="ImageURL" name="NewImageURL">
					<div class="clearfix"></div>
				</div>


				<input type="submit" value="Upload image"> 
				<input type="submit" value="Save" name="Save">

			</div>
</form>
		</div>
	</button>

	<button class="btn btn-danger dropdown" style="margin-left: 10%">
		Manage categories
		<div class="dropdown-content">

			<div class="form-w3agile form1">

				<div class="key">
					<input type="text" value="Name" name="Name">
					<div class="clearfix"></div>
				</div>
				<input type="submit" value="Create category">
				<div class="key">
					<input type="text" value="Existing category"
						name="ExistingCatrgory">
					<div class="clearfix"></div>
				</div>
				<div class="key">
					<ul style='overflow: auto; color: black; list-style-type: none;'>
						<li>Category1</li>
						<li>Category2</li>
						<li>Category3</li>
						<li>Category4</li>
					</ul>
				</div>

				<input type="submit" value="Modify">

			</div>


		</div>
	</button>

	<div class="check-out">
		<div class="w3-sidebar w3-light-grey w3-bar-block"
			style="width: 10%; height: 40%;">
			<h3 class="w3-bar-item">Sort</h3>
			<div class="w3-bar-item">Category</div>
			<div class="w3-bar-item">
				<select>
					<option value="Category1">Category1</option>
					<option value="Category2">Category2</option>
					<option value="Category3">Category3</option>
					<option value="Category4">Category4</option>
				</select>
			</div>
			<div class="w3-bar-item">Name</div>
			<div class="w3-bar-item">
				<input class="round-corner" type='text' placeholder="Name"
					style='width: 100%'>
			</div>
			<div class="w3-bar-item">Price</div>
			<div class="w3-bar-item">
				<input class="round-corner" type='text' placeholder="Min"
					style='width: 30%'>-<input class="round-corner" type='text'
					placeholder="Max" style='width: 30%'>
			</div>
			<div class="w3-bar-item">
				<button class="btn btn-danger">Sort</button>
			</div>
		</div>
		<div class="container" style="margin-left: 10%">
			<div class="spec ">
				<h3>Products</h3>
				<div class="ser-t">
					<b></b> <span><i></i></span> <b class="line"></b>
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<th class="t-head head-it ">Products</th>
						<th class="t-head">Price</th>
						<th class="t-head">Stock</th>
					</tr>
					<c:forEach var="product" items="${listProducts}">
						<form action="ManageProductsController" method="post">
							<tr>
								<td class="t-data"><a href="#" class="at-in"> <img
										src="$(product.imageURL)" class="img-responsive" alt="">
								</a>
									<div class="sed">
										<h5>${product.name}</h5>
									</div>
									<div class="clearfix"></div>
									<div class="manage-link">


										<div class="form-w3agile form1">


											<div class="key">
												<input type="text" value="Name" name="Name">
												<div class="clearfix"></div>
											</div>
											<div class="key">
												<input type="text" value="Description" name="Description">
												<div class="clearfix"></div>
											</div>
											<div class="key">
												<input type="text" value="Price" name="Price">
												<div class="clearfix"></div>
											</div>
											<div class="key">
												<input type="text" value="Stock" name="Stock">
												<div class="clearfix"></div>
											</div>
											<div>
												<a href="#"> <img src="images/wi.png"
													class="img-responsive" alt="">
												</a>
											</div>

											<input type="submit" value="Upload image"> <input
												type="submit" value="Update" name="Update">



										</div>
									</div></td>
								<td class="t-data">${product.price}</td>
								<td class="t-data">${product.stock}</td>
							</tr>
							</from>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>



</body>
</html>