<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>

<style>
.dropdown {
	position: relative;
	display: inline-block;
}

.dropdown-content {
	display: none;
	position: absolute;
	background-color: #f9f9f9;
	height: 600px;
	width: 400px;
	overflow: auto;
	box-shadow: 0px 8px 16px 0px rgba(0, 0, 0, 0.2);
	padding: 12px 16px;
	z-index: 1;
}

.dropdown:hover .dropdown-content {
	display: block;
}
</style>
</head>

<body>

  			 <sql:setDataSource var="myDS" driver="com.mysql.jdbc.Driver"
  		        url="jdbc:mysql://localhost/wad" user="root" password="" />
  		        
  			<sql:query var="listUser"   dataSource="${myDS}">
  		        SELECT * FROM user WHERE name = ? and surname = ? and country = ? and city = ? and role = ?
  		        <sql:param value = "${globalName}" />
  		        <sql:param value = "${globalSurname}" />
  		        <sql:param value = "${globalCountry}" />
  		        <sql:param value = "${globalCity}" />
  		        <sql:param value = "${globalRole}" />
  		    </sql:query>

	<div class="header">

		<div class="container">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="ManageUserScrollStateController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="manageorders" name="manageorders" class='btn btn-sm btn-primary-outline' id='manageOrders'><span class="glyphicon glyphicon-log-in"></span> Manage Orders</button></li>
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					</form>					
				</ul>		
			</div>
		</div>
		</div>
	</div>

	<button class="btn btn-danger dropdown" style="margin-left: 10%">
		Add new admin
		<div class="dropdown-content">
			<
			<form action="ManageUserController" method="post">
				<div class="form-w3agile form1">
					<h3>Register</h3>

					<div class="key">
						<input type="text" value="Name" name="Name">
						<div class="clearfix"></div>
					</div>
					<div class="key">
						<input type="text" value="Surname" name="Surname">
						<div class="clearfix"></div>
					</div>
					<div class="key">
						<input type="text" value="Email" name="Email">
						<div class="clearfix"></div>
					</div>
					<div class="key">
						<input type="text" value="Country" name="Country">
						<div class="clearfix"></div>
					</div>
					<div class="key">
						<input type="text" value="City" name="City">
						<div class="clearfix"></div>
					</div>
					<div class="key">
						<input type="text" value="Address" name="Address">
						<div class="clearfix"></div>
					</div>
					<input type="submit" name="addUser" value="Add user">
				</div>
			</form>

		</div>
	</button>
	

		<div class="w3-sidebar w3-light-grey w3-bar-block" style="width:10%">
			<form action="ManageUserController" method="post">
			
				<div class="w3-bar-item">Name</div>
 	 			<input type="hidden" name="email" value="${globalEmail}" />
   				<div class="w3-bar-item"><input value="" name="SortName" class="round-corner" type='text' placeholder="sortName" style='width:60%'></div>

				
				<div class="w3-bar-item">Surname</div>
 				<input type="hidden" name="email" value="${globalEmail}" />
   				<div class="w3-bar-item"><input value="" name="SortSurname" class="round-corner" type='text' placeholder="sortSurname" style='width:60%'></div>
						
				 <div class="w3-bar-item">Country</div>
 	 			<input type="hidden" name="email" value="${globalEmail}" />
   				<div class="w3-bar-item"><input value="" name="SortCountry" class="round-corner" type='text' placeholder="sortCountry" style='width:60%'></div>
   				 				
   	 			<div class="w3-bar-item">City</div>
 	 			<input type="hidden" name="email" value="${globalEmail}" />
   				<div class="w3-bar-item"><input value="" name="SortCity" class="round-corner" type='text' placeholder="sortCity" style='width:60%'></div>
   	
   	 			<div class="w3-bar-item">Role</div>
 	 			<input type="hidden" name="email" value="${globalEmail}" />
   				<div class="w3-bar-item"><input value="" name="SortRole" class="round-corner" type='text' placeholder="sortRole" style='width:60%'></div>
				
				<input type="hidden" name="email" value="${globalEmail}" />		
  				<div class="w3-bar-item"><button type="submit" value="sort" name="sort" class="btn btn-danger">Sort</button></div>
				</form>
		</div>
		
  
	<div class="container" style="margin-left: 10%">
			<div class="spec">
				<h3>Users</h3>
				<div class="ser-t">
					<b></b> <span><i></i></span> <b class="line"></b>
				</div>
			</div>
			<table class="table">
				<tbody>
					<tr>
						<th class="t-head"></th>
						<th class="t-head">Name</th>
						<th class="t-head">Surname</th>
						<th class="t-head">Email</th>
						<th class="t-head">Country</th>
						<th class="t-head">City</th>
						<th class="t-head">Role</th>
					</tr>
					<c:forEach var="users" items="${listUser.rows}">
						<form action="ManageUserScrollStateController" method="post">
							<tr>

								<td class="t-data"><input type="hidden" name="deleteEmail"
									value="${users.email}" />
									<button stype="submit" value="delete" name="delete"
										class='btn btn-sm btn-primary-outline'>
										<span class="glyphicon glyphicon-trash"></span>
									</button></td>
								<td class="t-data">${users.name}</td>
								<td class="t-data">${users.surname}</td>
								<td class="t-data">${users.email}</td>
								<td class="t-data">${users.country}</td>
								<td class="t-data">${users.city}</td>
								<td class="t-data">${users.role}</td>
							</tr>
						</form>
					</c:forEach>
				</tbody>
			</table>
		</div>




</body>
</html>