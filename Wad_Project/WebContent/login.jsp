<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>

<title>Login</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>

</head>
<body>

<div class="header">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">  
					<input type="hidden" name="email" value="${globalEmail}" />		      
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</form>				
				</ul>		
			</div>
		</div>

	</div>
			

     
<div class="login">
	
		<div class="main-agileits">
				<div class="form-w3agile">
					<h3>Login</h3>
					<form action="Service" method="post">
						<div class="key">
							<input  type="text" value="email" name="email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="password" value="password" name="password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
							<div class="clearfix"></div>
						</div>
						<input type="submit" value="login" name="login">
					</form>
				</div>
				<div class="forg">
					<form action="Service" method="post">
						<button type="submit" value="forgotpass" name="forgotpass" class='btn btn-sm btn-primary-outline forg-left' id='search'><span class="glyphicon glyphicon-search"></span> Forgot password </button>
						<button type="submit" value="register" name="register" class='btn btn-sm btn-primary-outline forg-right' id='search'><span class="glyphicon glyphicon-search"></span> Register </button>
					</form>
				<div class="clearfix"></div>
				</div>
			</div>
		</div>

</body>
</html>