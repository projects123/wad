<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>


<body>

<div class="header">

				<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">       
					<input type="hidden" name="email" value="${globalEmail}" />		 
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input  value="" name="searchName" class="round-corner" type='text' id='searchName' placeholder="searchName"></li>
					<input type="hidden" name="email" value="${globalEmail}" />	
					<li><button type="submit" value="search" name="search" class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<c:choose>
					<c:when test="${globalLoginStatus == 'true'}">
					<li><button type="submit" value="logoff" name="logoff" class='btn btn-sm btn-primary-outline' id='logoff'><span class="glyphicon glyphicon-log-in"></span> Logoff </button></li>	
					<input type="hidden" name="email" value="${globalEmail}" />	
					</c:when>
					<c:otherwise>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>					
					</c:otherwise>
					</c:choose>
					
					</form>					
				</ul>		
			</div>
		</div>

	</div>

			<div class="container">
				<form action="ProfileController" method="post">       
				<div class="forg">
					<div class="forg-left main-agileits" style='width:45%'>
						<div class="login">
							<div class="form-w3agile form1">
								<h3>User information</h3>
							
								<div class="key">
							<input  type="text" value=${user.name} name="Name">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value=${user.surname} name="Surname">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value=${user.email} name="Email">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value=${user.country} name="Country">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value=${user.city} name="City">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value=${user.address} name="Address">
							<div class="clearfix"></div>
						</div>
						<input type="submit" value="Save" name="Save">
							</div>							
						</div>
							
					</div>
			
					<div class="forg-right main-agileits" style='width:45%'>
						<div class="login">
		
							<div class="form-w3agile form1">
								<h3>Change Password</h3>
							
								<div class="key">
									<input  type="password" value="Old password" name="OldPassword">
									<div class="clearfix"></div>
								</div>
								<div class="key">
									<input  type="password" value="New password" name="Password">
									<div class="clearfix"></div>
								</div>
								<div class="key">
									<input  type="password" value="Confirm password" name="ConfirmPassword">
									<div class="clearfix"></div>
								</div>
								<input type="submit" value="Change" name="Change">
					
							</div>
				
						</div>
						<div class="clearfix"> </div>
					</div>
		 

				</div>
				</form>	
				<div class="clearfix"> </div>
			</div>					
<!--div class="container">
	
		<div class="forg">
				<div class="forg-left">
					<h3>Profile information</h3>
					<form action="#" method="post">
						<div class="key">
							<input  type="text" value="Email" name="Email" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Email';}" required="">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="password" value="Password" name="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
							<div class="clearfix"></div>
						</div>
						<input type="submit" value="Login">
					</form>
				</div>
				<div class="forg-right">
					<h3>Change password</h3>
					<button class='btn btn-sm btn-primary-outline forg-left' id='search'><span class="glyphicon glyphicon-search"></span> Forgot password </button>
					<button class='btn btn-sm btn-primary-outline forg-right' id='search'><span class="glyphicon glyphicon-search"></span> Register </button>
				
				<div class="clearfix"></div>
				</div>
			</div>
		</div-->
</body>
</html>