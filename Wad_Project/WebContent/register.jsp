<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Register</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>

</head>
<body>
<div class="header">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="RegisterController" method="post">        
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input class="round-corner" type='text' id='searchText' placeholder="Search"></li>
					<li><button class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>
					</form>					
				</ul>		
			</div>
		</div>

	</div>
			

     

<div class="login">
		<div class="main-agileits">
				<div class="form-w3agile form1">
					<h3>Register</h3>
					<form action="RegisterController" method="post">
						<div class="key">
							<input  type="text" value="Name" name="Name">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value="Password" name="Password">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value="Surname" name="Surname">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value="Email" name="Email">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value="Country" name="Country">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value="City" name="City">
							<div class="clearfix"></div>
						</div>
						<div class="key">
							<input  type="text" value="Address" name="Address">
							<div class="clearfix"></div>
						</div>
						<input type="submit" value="submit" name = "submit">
					</form>
				</div>
				
			</div>
		</div>
		
</body>
</html>