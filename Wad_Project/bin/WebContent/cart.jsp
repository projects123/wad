<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Home</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/style.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/font-awesome.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/montserrat.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/notosans.css"></link>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/w3.css"></link>

</head>
<body>



  			 <sql:setDataSource var="myDS" driver="com.mysql.jdbc.Driver"
  		        url="jdbc:mysql://localhost/wad" user="root" password="" />
  		        
  			<sql:query var="listCart"   dataSource="${myDS}">
  		        SELECT * FROM cart
  		    </sql:query>

<div class="header">

		<div class="container">
			
			<div class="head-t">
				<ul class="card">
					<form action="HomeController" method="post">        
					<li><button type="submit" value="home" name="home" class='btn btn-sm btn-primary-outline' id='home'><span class="glyphicon glyphicon-home"></span> Home </button></li>				                            
					<li><input class="round-corner" type='text' id='searchText' placeholder="Search"></li>
					<li><button class='btn btn-sm btn-primary-outline' id='search'><span class="glyphicon glyphicon-search"></span> Search </button></li>
					<li><button type="submit" value="login" name="login" class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-log-in"></span> Login </button></li>
					</form>					
				</ul>		
			</div>
		</div>
					<div class="check-out">	 
		<div class="container">	 
	 <div class="spec ">
				<h3>Wishlist</h3>
					<div class="ser-t">
						<b></b>
						<span><i></i></span>
						<b class="line"></b>
					</div>
			</div>
			
			
			
 <table class="table ">
		  <tr>
			<th class="t-head head-it ">Products</th>
			<th class="t-head">Price</th>
			<th class="t-head">Quantity</th>
			<th class="t-head">Total price</th>
		  </tr>
		  <tr>
		  
		  <c:forEach var="cart" items="${listCart.rows}">
			<td class="ring-in t-data">
				<a href="#" class="at-in">
					<img src="${cart.imageURL}" class="img-responsive" alt="">
				</a>
			<div class="sed">
				<h5>${cart.productName}</h5>
			</div>
				<div class="clearfix"> </div>
				<div class="close1"> <button class='btn btn-sm btn-primary-outline' id='login'><span class="glyphicon glyphicon-trash"></span></button> </div>
			 </td>
			<td class="t-data">${cart.price}</td>
			<td class="t-data"><div class="quantity"> 
								<div class="quantity-select">            
									<div class="entry value-minus">&nbsp;</div>
										<div class="entry value"><span class="span-1">1</span></div>									
									<div class="entry value-plus active">&nbsp;</div>
								</div>
							</div>
			
			</td>
			<td class="t-data">${cart.price}</td>
			</c:forEach>
	</table>
		 </div>
		 
		 <label>Total</label>
		 <button>Order</button>
		 </div>
			

     

</body>
</html>