package com.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Product;

public class ProductMapper {

	public List<Product> getList(ResultSet resultSet) {
		List<Product> products = new ArrayList<>();
		if(resultSet == null) {
			return products;
		}
 		try {
			while(resultSet.next()) {
				Product product = new Product();
				
				product.setName(resultSet.getString("name"));
				product.setPrice(resultSet.getString("price"));
				product.setImageURL(resultSet.getString("imageUrl"));
				product.setDescription(resultSet.getString("description"));
				product.setCategory(resultSet.getString("category"));
				
				products.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return products;
	}

}
