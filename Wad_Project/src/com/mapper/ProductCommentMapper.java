package com.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.ProductComment;

public class ProductCommentMapper {

	public List<ProductComment> getList(ResultSet resultSet) {
		List<ProductComment> comments = new ArrayList<>();
		if(resultSet == null) {
			return comments;
		}
 		try {
			while(resultSet.next()) {
				ProductComment product = new ProductComment();
				
				product.setProductName(resultSet.getString("productName"));
				product.setComment(resultSet.getString("comment"));
				
				comments.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return comments;
	}

}
