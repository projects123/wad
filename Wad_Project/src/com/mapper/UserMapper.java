package com.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.User;

public class UserMapper {

	public List<User> getList(ResultSet resultSet) {
		List<User> users = new ArrayList<>();
		try {
			while (resultSet.next()) {

				User user = new User();
				user.setName(resultSet.getString("name"));
				user.setSurname(resultSet.getString("surname"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCountry(resultSet.getString("country"));
				user.setCity(resultSet.getString("city"));
				user.setAddress(resultSet.getString("address"));
				user.setRole(resultSet.getString("role"));

				users.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return users;
	}
}
