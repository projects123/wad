package com.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.model.Order;
import com.model.OrderProduct;
import com.model.Product;

public class OrderProductMapper {

	public List<OrderProduct> getList(ResultSet resultSet) {
		List<OrderProduct> products = new ArrayList<>();
		if(resultSet == null) {
			return products;
		}
 		try {
			while(resultSet.next()) {
				OrderProduct product = new OrderProduct();
				
				product.setName(resultSet.getString("productName"));
				product.setPrice(resultSet.getDouble("productPrice"));
				product.setTotalPrice(resultSet.getDouble("productTotalPrice"));
				product.setQuantity(resultSet.getInt("productQuantity"));
				
				products.add(product);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return products;
	}

}
