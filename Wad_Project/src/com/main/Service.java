package com.main;


import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.OrderDAO;
import com.dao.UserDAO;
import com.model.Order;
import com.model.User;



/**
 * Servlet implementation class Service
 */
public class Service extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userdao;
	private OrderDAO orderDAO;
	public static Connection conn;
	static final String DB_URL = "jdbc:mysql://localhost/wad";
	 // Database credentials
	 static final String USER = "root";
	 static final String PASS = "";
	 
    public Service() throws ClassNotFoundException, SQLException {
        super();
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                 
        userdao = new UserDAO(conn);
        orderDAO = new OrderDAO(conn);
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String forgotpass = request.getParameter("forgotpass");
		String register = request.getParameter("register");
		String login = request.getParameter("login");
		String home = request.getParameter("home");
		
		if(login != null && login.equals("login")){
			
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            
            if (email != null && password != null){
                User user = new User();            
                user = userdao.getUser(email);        
            
                if(user != null && user.getPassword().equals(password)){      
                	request.setAttribute("globalEmail", email);
                	request.setAttribute("globalLoginStatus", "true");
                	
                	if(user.getRole().equals("admin")){
                		request.setAttribute("listUser", userdao.getAllUsers());
                        RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
                        req.include(request, response);
                	}
                	else{
                        RequestDispatcher req = request.getRequestDispatcher("home.jsp");
                        req.include(request, response);
                	}


                }
                else{
                    RequestDispatcher req = request.getRequestDispatcher("login.jsp");
                    req.include(request, response);
                }
            }
            

		}
		
		if(forgotpass != null && forgotpass.equals("forgotpass")){
			RequestDispatcher req = request.getRequestDispatcher("forgotpass.jsp");
			req.include(request, response);
		}
		
		if(register != null && register.equals("register")){
			RequestDispatcher req = request.getRequestDispatcher("register.jsp");
			req.include(request, response);
		}
		
		if(home != null && home.equals("home")){
//			request.setAttribute("unprocessedOrders", orderDAO.getAllOrdersFor("Email12"));
//			request.setAttribute("processedOrders", orderDAO.getAllOrdersFor("Email12"));
//			request.setAttribute("orders", orderDAO.getAllOrdersFor("Email12"));
			request.setAttribute("unprocessedOrders", orderDAO.getUnprocessedOrders());
			request.setAttribute("processedOrders", orderDAO.getProcessedOrders());
			RequestDispatcher req = request.getRequestDispatcher("manageOrders.jsp");
//			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}
	}

}
