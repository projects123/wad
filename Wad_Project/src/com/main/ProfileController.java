package com.main;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDAO;
import com.model.User;




/**
 * Servlet implementation class Service
 */
public class ProfileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO = new UserDAO(Service.conn);
	private User user;
	private Object oldPassword;

    public ProfileController() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String login = request.getParameter("login");
		String home = request.getParameter("home");
		String submit = request.getParameter("Save");
		String change = request.getParameter("Change");
		
		
		
		
		if(login != null && login.equals("login")){
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(home != null && home.equals("home")){
			
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
			
		}
		
		if(submit != null && submit.equals("Save")){
			user = new User();
			
			user.setName(request.getParameter("Name"));
			user.setSurname(request.getParameter("Surname"));
			user.setEmail(request.getParameter("Email"));
			user.setCountry(request.getParameter("Country"));
			user.setCity(request.getParameter("City"));
			user.setAddress(request.getParameter("Address"));
	
			
			userDAO.updateUser(user);
			String email = request.getParameter("Email");
			request.setAttribute("globalEmail", email );
        	request.setAttribute("globalLoginStatus", "true");
        	
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}
		
		if(change != null && change.equals("Change")) {
			user = new User();
			
			 if(request.getParameter("NewPassword").equals( request.getParameter("ConfirmPassword"))) {
			 	user.setPassword(request.getParameter("Password"));
			 }
			 
			 userDAO.updatePassword(user);
			 String email = request.getParameter("Email");
			 request.setAttribute("globalEmail", email );
	         request.setAttribute("globalLoginStatus", "true");
	        	
			 RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			 req.include(request, response);
			}
		}
		
	}


