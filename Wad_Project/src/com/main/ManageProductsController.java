package com.main;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ProductDAO;
import com.dao.UserDAO;
import com.model.Product;
import com.model.User;

public class ManageProductsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userdao;
	private ProductDAO productDAO;
	public static Connection conn;
	static final String DB_URL = "jdbc:mysql://localhost/wad";
	 // Database credentials
	 static final String USER = "root";
	 static final String PASS = "";
	 
    public ManageProductsController() throws ClassNotFoundException, SQLException {
        super();
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                 
        productDAO = new ProductDAO(conn);
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String addProduct = request.getParameter("Save");
		String updateProduct = request.getParameter("Update");
		//String sort = request.getParameter("sort");
		String home = request.getParameter("home");
		
		if(home != null && home.equals("login")){
			
            String email = request.getParameter("email");
            String password = request.getParameter("password");
            
            if (email != null && password != null){
                User user = new User();            
                user = userdao.getUser(email);        
            
                if(user != null && user.getPassword().equals(password)){      
                	request.setAttribute("globalEmail", email);
                    RequestDispatcher req = request.getRequestDispatcher("home.jsp");
                    req.include(request, response);
                }
                else{
                    RequestDispatcher req = request.getRequestDispatcher("login.jsp");
                    req.include(request, response);
                }
            }
            

		}
		
		
		
		if(updateProduct != null && updateProduct.equals("Update")){
			
			String name = request.getParameter("Name");
			String description = request.getParameter("Description");
			String price = request.getParameter("Price");
			String stock = request.getParameter("Stock");
			
			
			Product product = new Product();
			product.setName(name);
			product.setDescription(description);
			product.setPrice(price);
			product.setStock(stock);
			
			
			productDAO.updateProduct(product);
			request.setAttribute("listProducts", productDAO.getProductsContatiningName(""));
			RequestDispatcher req = request.getRequestDispatcher("manageProducts.jsp");
			req.include(request, response);
		}
		
		if(addProduct != null && addProduct.equals("Save")){
			String name = request.getParameter("NewName");
			String description = request.getParameter("NewDescription");
			String price = request.getParameter("NewPrice");
			String stock = request.getParameter("NewStock");
			String imageURL = request.getParameter("NewImageURL");
			
			
			Product product = new Product();
			product.setName(name);
			product.setDescription(description);
			product.setPrice(price);
			product.setStock(stock);
			product.setImageURL(imageURL);
			
			productDAO.createProduct(product);
			request.setAttribute("listProducts", productDAO.getProductsContatiningName(""));
			RequestDispatcher req = request.getRequestDispatcher("manageProducts.jsp");
			req.include(request, response);
		}
		/*if(sort != null && sort.equals("Sort")){
			String name = request.getParameter("sortName");;
			String surname = request.getParameter("sortSurname");;
			String country = request.getParameter("sortCountry");;
			String city = request.getParameter("sortCity");
			request.setAttribute("listUsers", userdao.sort(name, surname, country, city));
			RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
			req.include(request, response);
		}*/
	}

}
