package com.main;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.OrderDAO;
import com.dao.UserDAO;
import com.model.User;

public class ManageUserScrollStateController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userdao;
	public static Connection conn;
	static final String DB_URL = "jdbc:mysql://localhost/wad";
	 // Database credentials
	 static final String USER = "root";
	 static final String PASS = "";

		private OrderDAO orderDAO;
	 
    public ManageUserScrollStateController() throws ClassNotFoundException, SQLException {
        super();
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                 
        userdao = new UserDAO(conn);
        orderDAO = new OrderDAO(conn);
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String delete = request.getParameter("delete");
		String addUser = request.getParameter("addUser");
		String sort = request.getParameter("sort");
		String home = request.getParameter("home");
        String email = request.getParameter("email");
		String search = request.getParameter("search");
		String logoff = request.getParameter("logoff");

		String manageOrders = request.getParameter("manageorders");
		
		request.setAttribute("globalEmail", email);	
		
		if(!email.isEmpty()){
        	request.setAttribute("globalLoginStatus", "true");
		}
		
		if(home != null && home.equals("home")){			
			request.setAttribute("globalEmail", email);
            RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
            req.include(request, response);
			
		}
		
		
		if(manageOrders != null && manageOrders.equals("manageorders")){
			request.setAttribute("unprocessedOrders", orderDAO.getUnprocessedOrders());

			request.setAttribute("processedOrders", orderDAO.getProcessedOrders());
            RequestDispatcher req = request.getRequestDispatcher("manageOrders.jsp");
            req.include(request, response);
		}
		
		
		if(logoff != null && logoff.equals("logoff")){
        	request.setAttribute("globalLoginStatus", "false");
        	request.setAttribute("globalEmail", null);
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(search != null && search.equals("search")){
			String nameSearch = request.getParameter("searchName");

			request.setAttribute("globalSearch", nameSearch);
			
			RequestDispatcher req = request.getRequestDispatcher("homeScrollState.jsp");
			req.forward(request, response);
		}	
		
		if(delete != null && delete.equals("delete")){
			
			email = request.getParameter("deleteEmail");
			
			userdao.deteleUserWithEmail(email);
			request.setAttribute("listUsers", userdao.getAllUsers());
			RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
			req.include(request, response);
		}
		
		if(addUser != null && addUser.equals("Add user")){
			String name = request.getParameter("Name");
			String surname = request.getParameter("Surname");
			email = request.getParameter("Email");
			String country = request.getParameter("Country");
			String city = request.getParameter("City");
			String address = request.getParameter("Address");
			
			User user = new User();
			user.setName(name);
			user.setSurname(surname);
			user.setAddress(address);
			user.setEmail(email);
			user.setCity(city);
			user.setCountry(country);
			
			userdao.createUser(user);
			request.setAttribute("listUsers", userdao.getAllUsers());
			RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
			req.include(request, response);
		}
		
		if(sort != null && sort.equals("sort")){
			String name = request.getParameter("SortName");
			String surname = request.getParameter("SortSurname");
			String country = request.getParameter("SortCountry");
			String city = request.getParameter("SortCity");
			String role = request.getParameter("SortRole");

			request.setAttribute("globalName", name);
			request.setAttribute("globalSurname", surname);
			request.setAttribute("globalCountry", country);
			request.setAttribute("globalCity", city);
			request.setAttribute("globalRole", role);
			
			RequestDispatcher req = request.getRequestDispatcher("manageUserScrollState.jsp");
			req.forward(request, response);
		}	
	}

}
