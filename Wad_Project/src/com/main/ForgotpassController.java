package com.main;


import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDAO;
import com.model.User;



/**
 * Servlet implementation class Service
 */
public class ForgotpassController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO;

    public ForgotpassController() {
        super();
        userDAO = new UserDAO(Service.conn);
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String login = request.getParameter("login");
		String home = request.getParameter("home");
		String retrieve = request.getParameter("retrieve");
		
		if(login != null && login.equals("login")){
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(home != null && home.equals("home")){
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}
		
		if(retrieve != null && retrieve.equals("retrieve")){
			String email = request.getParameter("email");
			User user = userDAO.getUser(email);
			user.setPassword("1234");
			userDAO.updateUser(user);
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}

		
	}

}
