package com.main;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.OrderDAO;
import com.dao.UserDAO;
import com.model.User;

public class ManageOrdersController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private OrderDAO orderDAO;
	public static Connection conn;
	static final String DB_URL = "jdbc:mysql://localhost/wad";
	 // Database credentials
	 static final String USER = "root";
	 static final String PASS = "";
	 
    public ManageOrdersController() throws ClassNotFoundException, SQLException {
        super();
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                 
        orderDAO = new OrderDAO(conn);
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String remove = request.getParameter("Remove");
		String accept = request.getParameter("Accept");
		String home = request.getParameter("home");
		
		if(home != null && home.equals("login")){
			
//            String email = request.getParameter("email");
//            String password = request.getParameter("password");
//            
//            if (email != null && password != null){
//                User user = new User();            
//                user = userdao.getUser(email);        
//            
//                if(user != null && user.getPassword().equals(password)){      
//                	request.setAttribute("globalEmail", email);
//                    RequestDispatcher req = request.getRequestDispatcher("home.jsp");
//                    req.include(request, response);
//                }
//                else{
//                    RequestDispatcher req = request.getRequestDispatcher("login.jsp");
//                    req.include(request, response);
//                }
//            }
//            

		}
		
		
		if(remove != null && remove.equals("Remove")){
			Integer orderId = new Integer(request.getParameter("orderId"));
			orderDAO.declineOrder(orderId);
//			request.setAttribute("unprocessedOrders", orderDAO.getAllOrdersFor("Email12"));
//			request.setAttribute("processedOrders", orderDAO.getAllOrdersFor("Email12"));
			request.setAttribute("unprocessedOrders", orderDAO.getUnprocessedOrders());
			request.setAttribute("processedOrders", orderDAO.getProcessedOrders());
			RequestDispatcher req = request.getRequestDispatcher("manageOrders.jsp");
			req.include(request, response);
		}
		
		if(accept != null && accept.equals("Accept")){
			Integer orderId = new Integer(request.getParameter("orderId"));
			orderDAO.acceptOrder(orderId);
//			request.setAttribute("unprocessedOrders", orderDAO.getAllOrdersFor("Email12"));
//			request.setAttribute("processedOrders", orderDAO.getAllOrdersFor("Email12"));
			request.setAttribute("unprocessedOrders", orderDAO.getUnprocessedOrders());
			request.setAttribute("processedOrders", orderDAO.getProcessedOrders());
			RequestDispatcher req = request.getRequestDispatcher("manageOrders.jsp");
			req.include(request, response);
		}
	}

}
