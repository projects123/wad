package com.main;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ProductCommentDAO;
import com.dao.ProductDAO;
import com.dao.UserDAO;



/**
 * Servlet implementation class Service
 */
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO = new UserDAO(Service.conn);
	private ProductCommentDAO productCommentsDAO = new ProductCommentDAO(Service.conn);

    public HomeController() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String login = request.getParameter("login");
		String logoff = request.getParameter("logoff");
		String home = request.getParameter("home");
		String sort = request.getParameter("sort");
		String produce = request.getParameter("produce");
		String cart = request.getParameter("cart");
		String email = request.getParameter("email");
		String search = request.getParameter("search");
		String profile = request.getParameter("profile");
		
		request.setAttribute("globalEmail", email);	
		
		if(!email.isEmpty()){
        	request.setAttribute("globalLoginStatus", "true");
		}
		
		if(logoff != null && logoff.equals("logoff")){
        	request.setAttribute("globalLoginStatus", "false");
        	request.setAttribute("globalEmail", null);
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}

		
		if(login != null && login.equals("login")){
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(home != null && home.equals("home")){
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);

		}	
		
		if(sort != null && sort.equals("sort")){
			String category = request.getParameter("category");
			//String name = request.getParameter("name");
			String priceMin = request.getParameter("priceMin");
			String priceMax = request.getParameter("priceMax");

			request.setAttribute("globalCategory", category);
			//request.setAttribute("globalName", 		name);
			request.setAttribute("globalPriceMin", priceMin);
			request.setAttribute("globalPriceMax", priceMax);
			
			RequestDispatcher req = request.getRequestDispatcher("homeScrollState.jsp");
			req.forward(request, response);
		}	
		
		if(search != null && search.equals("search")){
			String nameSearch = request.getParameter("searchName");

			request.setAttribute("globalSearch", nameSearch);
			
			RequestDispatcher req = request.getRequestDispatcher("homeScrollState.jsp");
			req.forward(request, response);
		}	
		
		if(produce != null && produce.equals("produce")){
			String productName = request.getParameter("productName");
			String price = request.getParameter("price");
			String imagine = request.getParameter("imagine");
			String description = request.getParameter("description");

			request.setAttribute("commetns", productCommentsDAO.getCommentsForProduct(productName));
			request.setAttribute("globalProductName", productName);
			request.setAttribute("globalImagine", imagine);
			request.setAttribute("globalPrice", price);
			request.setAttribute("globalDescription", description);
			
			RequestDispatcher req = request.getRequestDispatcher("produce.jsp");
			req.forward(request, response);
		}	
		
		if(cart != null && cart.equals("cart")){
			String productName = request.getParameter("productName");
			String price = request.getParameter("price");
			String imagine = request.getParameter("imagine");

			request.setAttribute("globalProductName", productName);
			request.setAttribute("globalImagine", imagine);
			request.setAttribute("globalPrice", price);
			
			
			userDAO.addProductToCart(email, productName, imagine, price);
			
			RequestDispatcher req = request.getRequestDispatcher("cart.jsp");
			req.forward(request, response);
		}
		
		if(profile != null && profile.equals("profile")){
			
			request.setAttribute("user", userDAO.getUser(email));
			RequestDispatcher req = request.getRequestDispatcher("profile.jsp");
			req.include(request, response);
		}
	}
}
