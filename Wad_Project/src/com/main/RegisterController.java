package com.main;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.UserDAO;
import com.model.User;




/**
 * Servlet implementation class Service
 */
public class RegisterController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO = new UserDAO(Service.conn);

    public RegisterController() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String login = request.getParameter("login");
		String home = request.getParameter("home");
		String submit = request.getParameter("submit");
		
		
		
		if(login != null && login.equals("login")){
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(home != null && home.equals("home")){
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}
		
		if(submit != null && submit.equals("submit")){
			User user = new User();
			user.setName(request.getParameter("Name"));
			user.setSurname(request.getParameter("Surname"));
			user.setPassword(request.getParameter("Password"));
			user.setEmail(request.getParameter("Email"));
			user.setCountry(request.getParameter("Country"));
			user.setCity(request.getParameter("City"));
			user.setAddress(request.getParameter("Address"));
			
			userDAO.createUser(user);
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}
		
	}

}
