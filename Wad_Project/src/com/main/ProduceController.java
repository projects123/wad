package com.main;


import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.ProductCommentDAO;
import com.dao.UserDAO;
import com.model.User;




/**
 * Servlet implementation class Service
 */
public class ProduceController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userDAO = new UserDAO(Service.conn);
	private ProductCommentDAO productCommentsDAO = new ProductCommentDAO(Service.conn);


    public ProduceController() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String login = request.getParameter("login");
		String logoff = request.getParameter("logoff");
		String home = request.getParameter("home");
		String cart = request.getParameter("cart");
		String email = request.getParameter("email");
		String comment = request.getParameter("comment");
		
		request.setAttribute("globalEmail", email);	
		
		if(!email.isEmpty()){
        	request.setAttribute("globalLoginStatus", "true");
		}
		
		if(logoff != null && logoff.equals("logoff")){
        	request.setAttribute("globalLoginStatus", "false");
        	request.setAttribute("globalEmail", "false");
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		

		
		if(login != null && login.equals("login")){
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(home != null && home.equals("home")){
			RequestDispatcher req = request.getRequestDispatcher("home.jsp");
			req.include(request, response);
		}
		
		if(cart != null && cart.equals("cart")){
			String productName = request.getParameter("productName");
			String price = request.getParameter("price");
			String imagine = request.getParameter("imagine");

			request.setAttribute("globalProductName", productName);
			request.setAttribute("globalImagine", imagine);
			request.setAttribute("globalPrice", price);

			userDAO.addProductToCart(email, productName, imagine, price);

			RequestDispatcher req = request.getRequestDispatcher("cart.jsp");
			req.forward(request, response);
		}
		
		if(comment != null && comment.equals("comment")){
			String productName = request.getParameter("productName");
			String commentProduct = request.getParameter("commentProduct");
			String price = request.getParameter("price");
			String imagine = request.getParameter("imagine");
			String description = request.getParameter("description");
			
			request.setAttribute("commetns", productCommentsDAO.getCommentsForProduct(productName));
			request.setAttribute("globalProductName", productName);
			request.setAttribute("globalImagine", imagine);
			request.setAttribute("globalPrice", price);
			request.setAttribute("globalDescription", description);

			productCommentsDAO.addCommentsForProduct(productName, commentProduct);

			RequestDispatcher req = request.getRequestDispatcher("produce.jsp");
			req.forward(request, response);
		}
		
	}

}
