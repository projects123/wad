package com.main;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.OrderDAO;
import com.dao.ProductDAO;
import com.dao.UserDAO;
import com.model.User;

public class ManageUserController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private UserDAO userdao;
	private OrderDAO orderDAO;
	private ProductDAO productDAO;
	
	public static Connection conn;
	static final String DB_URL = "jdbc:mysql://localhost/wad";
	 // Database credentials
	 static final String USER = "root";
	 static final String PASS = "";
	 
    public ManageUserController() throws ClassNotFoundException, SQLException {
        super();
        
        Class.forName("com.mysql.jdbc.Driver");
        conn = DriverManager.getConnection(DB_URL, USER, PASS);
                 
        userdao = new UserDAO(conn);
        orderDAO = new OrderDAO(conn);
        productDAO = new ProductDAO(conn);
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub


	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String delete = request.getParameter("delete");
		String addUser = request.getParameter("addUser");
		String logoff = request.getParameter("logoff");
		String sort = request.getParameter("sort");
		String home = request.getParameter("home");
        String email = request.getParameter("email");
		String search = request.getParameter("search");
		String manageOrders = request.getParameter("manageorders");
		String manageProducts = request.getParameter("manageProducts");
		String profile = request.getParameter("profile");
		
		request.setAttribute("globalEmail", email);	
		
		if(email != null && !email.isEmpty()){
        	request.setAttribute("globalLoginStatus", "true");
		}
		
		if(profile != null && profile.equals("profile")){
            RequestDispatcher req = request.getRequestDispatcher("profile.jsp");
            req.include(request, response);
		}
		
		if(manageOrders != null && manageOrders.equals("manageorders")){
			request.setAttribute("unprocessedOrders", orderDAO.getUnprocessedOrders());

			request.setAttribute("processedOrders", orderDAO.getProcessedOrders());
            RequestDispatcher req = request.getRequestDispatcher("manageOrders.jsp");
            req.include(request, response);
		}
		
		if(manageProducts != null && manageProducts.equals("manageProducts")){
			request.setAttribute("listProducts", productDAO.getProductsContatiningName(""));

            RequestDispatcher req = request.getRequestDispatcher("manageProducts.jsp");
            req.include(request, response);
		}
		
		if(home != null && home.equals("home")){			
			request.setAttribute("globalEmail", email);
			request.setAttribute("listUser", userdao.getAllUsers());
            RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
            req.include(request, response);
			
		}
		
		if(logoff != null && logoff.equals("logoff")){
        	request.setAttribute("globalLoginStatus", "false");
        	request.setAttribute("globalEmail", null);
			RequestDispatcher req = request.getRequestDispatcher("login.jsp");
			req.include(request, response);
		}
		
		if(search != null && search.equals("search")){
			String nameSearch = request.getParameter("searchName");

			request.setAttribute("globalSearch", nameSearch);
			
			RequestDispatcher req = request.getRequestDispatcher("homeScrollState.jsp");
			req.forward(request, response);
		}	
		
		if(delete != null && delete.equals("delete")){
			
			email = request.getParameter("deleteEmail");
			
			userdao.deteleUserWithEmail(email);
			request.setAttribute("listUsers", userdao.getAllUsers());
			RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
			req.include(request, response);
		}
		
		if(addUser != null && addUser.equals("Add user")){
			String name = request.getParameter("NewName");
			String surname = request.getParameter("NewSurname");
			email = request.getParameter("NewEmail");
			String country = request.getParameter("NewCountry");
			String city = request.getParameter("NewCity");
			String address = request.getParameter("NewAddress");
			
			User user = new User();
			user.setName(name);
			user.setSurname(surname);
			user.setAddress(address);
			user.setEmail(email);
			user.setCity(city);
			user.setCountry(country);
			user.setRole("admin");
			user.setPassword("123");
			userdao.createUser(user);
			request.setAttribute("listUser", userdao.getAllUsers());
			RequestDispatcher req = request.getRequestDispatcher("manageUser.jsp");
			req.include(request, response);
		}
		
		if(sort != null && sort.equals("sort")){
			String name = request.getParameter("SortName");
			String surname = request.getParameter("SortSurname");
			String country = request.getParameter("SortCountry");
			String city = request.getParameter("SortCity");
			String role = request.getParameter("SortRole");

			request.setAttribute("globalName", name);
			request.setAttribute("globalSurname", surname);
			request.setAttribute("globalCountry", country);
			request.setAttribute("globalCity", city);
			request.setAttribute("globalRole", role);
			
			RequestDispatcher req = request.getRequestDispatcher("manageUserScrollState.jsp");
			req.forward(request, response);
		}	
	}

}
