package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.mapper.ProductMapper;
import com.model.Product;


public class ProductDAO {

	private Connection conn;
	private ProductMapper productMapper = new ProductMapper();
	
	

	public ProductDAO(Connection conn) {
		super();
		this.conn = conn;
	}

	public List<Product> getProductsContatiningName(String name) {
		String query = "SELECT * FROM product WHERE name LIKE ?";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, "%" + name + "%");
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return productMapper.getList(resultSet);
	}

	public Product getProductWithName(String name) {
		List<Product> productsContatiningName = getProductsContatiningName(name);
		if (productsContatiningName.isEmpty()) {
			return null;
		}
		return productsContatiningName.get(0);
	}

	public List<Product> getProductsByCategory(String categoryName) {
		String query = "SELECT * FROM product WHERE category=?";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, categoryName);
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return productMapper.getList(resultSet);
	}

	public List<Product> getProductsWithPriceBiggerThan(String price) {
		String query = "SELECT * FROM product WHERE price >=?";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			Double priceDouble = new Double(price);
			p.setDouble(1, priceDouble);
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return productMapper.getList(resultSet);
	}

	public void createProduct(Product product) {
		String query = "INSERT INTO product(name, description, price, stock, imageURL) VALUES (?,?,?,?,?)";
		
		try {
			PreparedStatement p = conn.prepareStatement(query);
			Double priceDouble = new Double(product.getPrice());
			
			p.setString(1, product.getName());
			p.setString(2, product.getDescription());
			p.setDouble(3, priceDouble);
			p.setString(5, product.getImageURL());

			p.setInt(4, new Integer (product.getStock()));
			
			 p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
		
	}

	public void updateProduct(Product product) {
	String query = "UPDATE product SET name=?, description=?, price=?, stock=? where name = ?";
		
		try {
			PreparedStatement p = conn.prepareStatement(query);
			Double priceDouble = new Double(product.getPrice());
			
			p.setString(1, product.getName());
			p.setString(2, product.getDescription());
			p.setDouble(3, priceDouble);
			p.setInt(4, new Integer (product.getStock()));
			p.setString(5, product.getName());
			
			 p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		
	}
	

}
