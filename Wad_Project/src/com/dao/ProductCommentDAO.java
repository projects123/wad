package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.mapper.ProductCommentMapper;
import com.model.ProductComment;

public class ProductCommentDAO {

	private ProductCommentMapper productCommentMapper = new ProductCommentMapper();
	private Connection conn;
	
	static final String DB_URL = "jdbc:mysql://localhost/wad";
	 // Database credentials
	 static final String USER = "root";
	 static final String PASS = "";

	public ProductCommentDAO(Connection conn1) {
		super();
		try {
			Class.forName("com.mysql.jdbc.Driver");
	        conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}



	public List<ProductComment> getCommentsForProduct(String productName) {
		String query = "SELECT * FROM productcomment WHERE productName=?";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, productName);
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return productCommentMapper.getList(resultSet);
	}
	
	public void addCommentsForProduct(String productName, String comment) {
		String query = "INSERT INTO productcomment (productName, comment) VALUES (?,?)";

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, productName);
			p.setString(2, comment);
			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
