package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mapper.OrderProductMapper;
import com.model.Order;

public class OrderDAO {

	private Connection conn;
	private OrderProductMapper orderProductMapper = new OrderProductMapper();

	
	public OrderDAO(Connection conn) {
		super();
		this.conn = conn;
	}


	public List<Order> getAllOrdersFor(String email) {
		String query = "SELECT * FROM orders WHERE userEmail=?";
		String productsQuery = "SELECT * FROM ordersproducts WHERE orderID=?";
		
		ResultSet resultSet = null;
		List<Order> orders = new ArrayList<>();

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, email);
			resultSet = p.executeQuery();

		while(resultSet.next()) {
			Order order = new Order();
			int id = resultSet.getInt("id");
			
			order.setPrice(resultSet.getDouble("totalPrice"));
			order.setId(id);
			order.setStatus(resultSet.getString("processStatus"));
			ResultSet productsResultSet = null;

			p = conn.prepareStatement(productsQuery);
			p.setInt(1, id);
			productsResultSet = p.executeQuery();
			order.setProducts(orderProductMapper.getList(productsResultSet));
			
			orders.add(order);
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return orders;
	}


	public void declineOrder(Integer orderId) {
		String query = "UPDATE `orders` SET `processStatus`='declined' WHERE id = ?";
		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setInt(1, orderId);
			p.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public void acceptOrder(Integer orderId) {
		String query = "UPDATE `orders` SET `processStatus`='processed' WHERE id = ?";
		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setInt(1, orderId);
			p.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public List<Order> getUnprocessedOrders() {
		String query = "SELECT * FROM orders WHERE processStatus='unprocessed'";
		String productsQuery = "SELECT * FROM ordersproducts WHERE orderID=?";
		
		ResultSet resultSet = null;
		List<Order> orders = new ArrayList<>();

		try {
			PreparedStatement p = conn.prepareStatement(query);
			resultSet = p.executeQuery();

		while(resultSet.next()) {
			Order order = new Order();
			int id = resultSet.getInt("id");
			
			order.setPrice(resultSet.getDouble("totalPrice"));
			order.setId(id);
			order.setStatus(resultSet.getString("processStatus"));
			ResultSet productsResultSet = null;

			p = conn.prepareStatement(productsQuery);
			p.setInt(1, id);
			productsResultSet = p.executeQuery();
			order.setProducts(orderProductMapper.getList(productsResultSet));
			
			orders.add(order);
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return orders;
	}


	public List<Order> getProcessedOrders() {
		String query = "SELECT * FROM orders WHERE processStatus='processed'";
		String productsQuery = "SELECT * FROM ordersproducts WHERE orderID=?";
		
		ResultSet resultSet = null;
		List<Order> orders = new ArrayList<>();

		try {
			PreparedStatement p = conn.prepareStatement(query);
			resultSet = p.executeQuery();

		while(resultSet.next()) {
			Order order = new Order();
			int id = resultSet.getInt("id");
			
			order.setPrice(resultSet.getDouble("totalPrice"));
			order.setId(id);
			order.setStatus(resultSet.getString("processStatus"));
			ResultSet productsResultSet = null;

			p = conn.prepareStatement(productsQuery);
			p.setInt(1, id);
			productsResultSet = p.executeQuery();
			order.setProducts(orderProductMapper.getList(productsResultSet));
			
			orders.add(order);
		}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return orders;

	}

}
