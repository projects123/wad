package com.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mysql.jdbc.Connection;

public class CategoryDAO {

	private Connection conn;
	
	

	public CategoryDAO(Connection conn) {
		super();
		this.conn = conn;
	}



	public List<String> getAllCategories() {
		String query = "SELECT * FROM category";
		ResultSet resultSet = null;

		List<String> categories = new ArrayList<>();
		try {
			PreparedStatement p = conn.prepareStatement(query);
			resultSet = p.executeQuery();
			
			while(resultSet.next()) {
				categories.add(resultSet.getString("category_name"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return categories;
	}
}
