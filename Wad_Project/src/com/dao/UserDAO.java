package com.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.mapper.UserMapper;
import com.model.User;

public class UserDAO {

	private Connection conn;
	private UserMapper userMapper = new UserMapper();

	static final String DB_URL = "jdbc:mysql://localhost/wad";
	// Database credentials
	static final String USER = "root";
	static final String PASS = "";

	public UserDAO(Connection conn1) {
		super();
		try {
			Class.forName("com.mysql.jdbc.Driver");
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public User getUser(String email) {
		String query = "SELECT * FROM user WHERE email=?";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, email);
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		List<User> result = userMapper.getList(resultSet);
		if (result.isEmpty()) {
			return null;
		}
		return result.get(0);
	}

	public void createUser(User user) {
		String query = "INSERT INTO `user`(`name`, `surname`, `password`, `email`, `country`, `city`, `address`, 'role') VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, user.getName());
			p.setString(2, user.getSurname());
			p.setString(3, user.getPassword());
			p.setString(4, user.getEmail());
			p.setString(5, user.getCountry());
			p.setString(6, user.getCity());
			p.setString(7, user.getAddress());
			p.setString(8, user.getRole());
			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void updateUser(User user) {

		String query = "UPDATE `user` SET `name`=?,`surname`=?,`email`=?,`country`=?,`city`=?,`address`=? WHERE email = ?";
		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, user.getName());
			p.setString(2, user.getSurname());
			p.setString(3, user.getEmail());
			p.setString(4, user.getCountry());
			p.setString(5, user.getCity());
			p.setString(6, user.getAddress());
			p.setString(7, user.getEmail());

			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void addProductToCart(String userEmail, String productName, String imageURL, String price) {
		String query = "INSERT INTO cart (email, productName, imageURL, price) VALUES (?,?,?,?)";

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, userEmail);
			p.setString(2, productName);
			p.setString(3, imageURL);
			p.setString(4, price);
			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void removeProductToCart(String userEmail,String productName,String imageURL,String price){
		String query = "DELETE FROM cart WHERE email = ? and productName = ? and imageURL = ? and price = ?";

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, userEmail);
			p.setString(2, productName);
			p.setString(3, imageURL);
			p.setString(4, price);
			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void deteleUserWithEmail(String email) {

		String query = "DELETE FROM user WHERE email=?";

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, email);
			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public List<User> getAllUsers() {

		String query = "SELECT * FROM user";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userMapper.getList(resultSet);
	}

	public List<User> sort(String name, String surname, String country, String city) {
		String query = "SELECT * FROM user WHERE name LIKE ? AND surname LIKE ? AND country LIKE ? AND city LIKE ?";
		ResultSet resultSet = null;

		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, "%" + name + "%");
			p.setString(2, "%" + surname + "%");
			p.setString(3, "%" + country + "%");
			p.setString(4, "%" + city + "%");
			
			resultSet = p.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return userMapper.getList(resultSet);
	}

	public void updatePassword(User user) {
		String query = "UPDATE `user` SET `password`=? WHERE email = ?";
		try {
			PreparedStatement p = conn.prepareStatement(query);
			p.setString(1, user.getPassword());
			p.setString(2, user.getEmail());

			p.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}		
	}

}
